from rest_framework import serializers, validators
from django.contrib.auth.models import User
from .models import Wallet, WalletTransaction
from rest_framework.authtoken.models import Token
from .utils import generate_wallet_id

# class UserSerializers(serializers.Serializer):
#     id = serializers.IntegerField()
#     username = serializers.CharField(max_length=255)
#     is_staff = serializers.BooleanField()
#     is_active = serializers.BooleanField()


class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'is_active',)

# class WalletSerializers(serializers.Serializer):
#     wallet_id = serializers.CharField(max_length=16)
#     balance = serializers.IntegerField()
#     user = UserSerializers()

class WalletSerializers(serializers.ModelSerializer):
    user = UserSerializers()
    class Meta:
        model = Wallet
        fields = ('id', 'wallet_id', 'balance', 'user')






class CreateTransactionSerializers(serializers.Serializer):

    to_wallet_id = serializers.CharField(max_length=16)
    amount = serializers.IntegerField()


class TransactionSerializers(serializers.ModelSerializer):
    class Meta:
        model = WalletTransaction
        fileds = '__all__'


class RegisterSerializers(serializers.Serializer):
    username = serializers.CharField(max_length=255, write_only=True)
    password1 = serializers.CharField(max_length=255, write_only=True)
    password2 = serializers.CharField(max_length=255, write_only=True)
    token = serializers.CharField(max_length=40, read_only=True)
    wallet = WalletSerializers(read_only=True)


    def validate(self, attrs):
        password1 = attrs.get('password1', None)
        password2 = attrs.get('password2', None)
        username = attrs.get('username', None)
        if password1 != password2:
            raise validators.ValidationError({'message':'Parolda hatolik bor'})
        if User.objects.filter(username=username).count() > 0:
            raise validators.ValidationError({"messasge": "bu user oldin ruyxatdan utgan"})

        # user_yaratish
        user = User(username=username)
        user.set_password(password1)
        user.save()

        # token_yaratish

        token = Token.objects.create(user=user)
        attrs.update({"token": token.key})

        # user__wallet_id_yaratish
        generated_wallet_id = generate_wallet_id()
        while True:
            if Wallet.objects.filter(wallet_id=generated_wallet_id).count() == 0:
                wallet_instance = Wallet.objects.create(wallet_id=generated_wallet_id, user=user)
                attrs.update({"wallet": WalletSerializers(wallet_instance).data})
                print(token.key)
                return attrs


